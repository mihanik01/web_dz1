from django.conf.urls import patterns, include, url
from say import views, view

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ask_krutyakov.views.home', name='home'),
    # url(r'^blog/', 'blog.urls'),
    url(r'^$', views.index),
    url(r'^search', views.search),
    url(r'^question/*$', views.question),
    url(r'^login/{0,1}$', views.login),
    url(r'^logout', views.logout_view),
    url(r'^settings/{0,1}$', views.signup),
    url(r'^sayhello/', view.hello),
    url(r'^register', views.register),
    url(r'^ask', views.ask),
    url(r'^delete_question', views.delete_question),
    url(r'^delete_answer', views.delete_answer),
    url(r'^admin/', admin.site.urls),
    url(r'^like', views.like),
    url(r'^set_correct', views.set_correct),
    url(r'^answer', views.answer),
    url(r'^tag/(?P<tagname>.*)', views.tag_list),
)
