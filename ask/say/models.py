from django.db import models
from django.contrib.auth.models import AbstractUser, User
from django import forms
from djangosphinx.models import SphinxSearch

class MyUser(AbstractUser):
  avatar = models.ImageField(upload_to='/avatar')
  rating = models.IntegerField()
  class Meta:
    db_table = 'user'

class UserForm(forms.ModelForm):
  class Meta:
    model = MyUser
    fields = ['username', 'email', 'avatar']

class RegisterForm(forms.ModelForm):
  password = forms.CharField(widget=forms.PasswordInput())
  class Meta:
    model = MyUser
    fields = ['username', 'email']

class Tag(models.Model):
  word = models.CharField(max_length = 30)

class Question(models.Model):
  title = models.CharField(max_length = 100)
  content = models.TextField()
  author = models.ForeignKey('MyUser')
  date = models.DateTimeField(auto_now_add = True)
  tags = models.ManyToManyField('Tag')
  rating = models.IntegerField(null=False, default=0)
  search = SphinxSearch(weights={'title':100, 'content': 50})

class Like(models.Model):
  user = models.ForeignKey('MyUser')
  question = models.ForeignKey('Question')

class Answer(models.Model):
  question = models.ForeignKey('Question')
  content = models.TextField()
  author = models.ForeignKey('MyUser')
  date = models.DateTimeField(auto_now_add = True)
  is_true = models.BooleanField()
  search = SphinxSearch(weights={'content':50})

class AnswerForm(forms.ModelForm):
  def __init__(self, *args, **kwargs):
    self.author = kwargs.pop('user', None)
    self.question = kwargs.pop('question', None)
    super(AnswerForm, self).__init__(*args, **kwargs)
    
  def save(self, commit=True):
    obj = super(AnswerForm, self).save(commit=False)
    obj.author = self.author
    obj.question = self.question
    if commit:
      obj.save()
    return obj
  
  class Meta:
    model = Answer
    exclude = ['author','date','is_true','question']

class AskForm(forms.ModelForm):
  tags = forms.CharField(max_length = 100, required = False)
  def __init__(self, *args, **kwargs):
    self.author = kwargs.pop('user', None)
    super(AskForm, self).__init__(*args, **kwargs)
    
  def save(self, commit=True):
    obj = super(AskForm, self).save(commit=False)
    obj.author = self.author
    if commit:
      obj.save()
    return obj
  
  class Meta:
    model = Question
    exclude = ['author','date','rating','tags']
