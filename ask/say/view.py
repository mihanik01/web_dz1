from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def hello(request):
  html="""
<!DOCTYPE html>
<html>
 <head>
  <title>Django hello page</title>
 </head>
 <body>
  <h1>Django hello page</h1>
   <h2>GET</h2>
    <ol>
"""
  for r in request.GET:
    html+="<li>"+r+"="+request.GET.get(r,'')+"</li>"
  html+="""
    </ol>
   <h2>POST</h2>
    <ol>"""
  for r in request.POST:
    html+="<li>"+r+"="+request.POST.get(r,'')+"</li>"
  html+="""
    </ol>
 </body>
</html>
"""
  return HttpResponse(html)
