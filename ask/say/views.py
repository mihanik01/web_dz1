from django.shortcuts import render, redirect
from models import *
from django.db.models import Count
from django.http import HttpResponseNotFound, HttpResponse
from django.contrib.auth import authenticate, logout
from django.core.paginator import Paginator, EmptyPage
from django.contrib.auth.decorators import login_required
from djangosphinx.models import SphinxQuerySet

import json

def index(request):
  url = request.path
  pageid = 1
  orderby = 'date'
  if request.GET.get('order') == 'rating':
    orderby = 'rating'
  url = url + "?order="+orderby
  if request.GET.get('page'):
    pageid = int(request.GET.get('page'))
  if pageid < 1:
    pageid = 1
  
  question_list = Question.objects.order_by('-'+orderby)
  paginator = Paginator(question_list, 20)
  
  try:
    questions = paginator.page(pageid)
  except EmptyPage:
    questions = paginator.page(paginator.num_pages)
  
  #for q in questions:
  #  liketype = "like"
  #  if q.like_set.filter(user = request.user).exists():
  #    liketype = "dislike"
  #  q.liketype=liketype

  pagenum_minus_one = questions.paginator.num_pages - 1
  params = {
    'question_list' : questions,
    'url': url,
    'pagenum_minus_one': pagenum_minus_one
  }
  return render(request, 'index.html', params)

def search(request):
  url = request.path
  pageid = 1
  if request.GET.get('page'):
    pageid = int(request.GET.get('page'))
  if pageid < 1:
    pageid = 1
  
  query = request.GET.get('q', False)
  question_list = Question.search.query(query).order_by('-@weight')
  if query:
    url = url+str(query)
  #answer_list = Answer.search.query(request.GET.get('q', False)).order_by('-@weight')
  #question_list = []
  #for answer in answer_list:
  #  question_list.append(answer.question)
  
  #question_list = (question_list | set(qansw_list))
  paginator = Paginator(question_list, 20)
  
  try:
    questions = paginator.page(pageid)
  except EmptyPage:
    questions = paginator.page(paginator.num_pages)

  pagenum_minus_one = questions.paginator.num_pages - 1
  params = {
    'question_list':questions,
    'url': url,
    'pagenum_minus_one': pagenum_minus_one,
    'query' : query
  }
  return render(request, 'search.html', params)

def question(request):
  question_id = 1;
  answer_id = 1;
  if request.GET.get('id'):
    question_id = int(request.GET.get('id'))
  if request.GET.get('answer_id'):
    answer_id = int(request.GET.get('answer_id'))
  
  if question_id < 1:
    question_id = 1
  if answer_id < 1:
    answer_id = 1
  q = Question.objects.get(id = question_id)
  c = q.content.split("\n")
  
  ans = q.answer_set.order_by('-date').all()
  paginator = Paginator(ans, 5)
  try:
    answers = paginator.page(answer_id)
  except EmptyPage:
    answers = paginator.page(paginator.num_pages)
    
  liketype = "like"
  if request.user.is_authenticated() and q.like_set.filter(user = request.user).exists():
    liketype = "dislike"
  
  url = "/question?id="+str(question_id)
  
  answer_form = None
  if request.user.is_authenticated():
    answer_form = AnswerForm()
  return render(request, 'question.html',
  {'question': q,
   'content': c,
   'answers': answers,
   'url' : url,
   'liketype' : liketype,
   'answer_form' : answer_form
  })
  
@login_required
def answer(request):
  result = {}
  result['status'] = 'FAIL'
  if request.method == 'POST':
    q = Question.objects.get(id=request.POST.get('q_id', False))
    answ_form = AnswerForm(request.POST, user=request.user, question=q)
    if q and answ_form.is_valid():
      answer = answ_form.save(False)
      answer.is_true = False
      answer.save()
      result['status'] = 'OK'
      result['username'] = request.user.username
      result['datetime'] = answer.date.strftime("%Y-%m-%d")
      result['userimg'] = request.user.avatar.url
      result['answ_id'] = answer.id
    elif not q:
      result['message'] = 'No such question'
    elif not answ_form.is_valid():
      result['message'] = 'Invalid form specified'
  return HttpResponse(json.dumps(result), 'application/json')

@login_required
def delete_answer(request):
  result = {}
  result['status'] = 'FAIL'
  answer = Answer.objects.get(id=request.POST.get('id', False))
  if answer and request.user == answer.author:
    result['status'] = 'OK'
    answer.delete()
  return HttpResponse(json.dumps(result), 'application/json')
    

from django.contrib.auth import get_user_model, login as user_login

def login(request):
  if request.method == 'POST':
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    user = authenticate(username = username, password = password)
    response_data = {}
    response_data['un'] = username
    response_data['psw'] = password
    if user is not None:
      user_login(request, user)
      response_data['status'] = 'OK'
      response_data['redirect'] = '/'
    else:
      response_data['status'] = 'FAIL'
      response_data['err_message'] = 'Wrong login/password'
    return HttpResponse(json.dumps(response_data), 'application/json')
  else:
    if request.user and request.user.is_authenticated():
      return redirect('/')
    return render(request, 'login.html')
  
def logout_view(request):
  logout(request)
  return redirect(request.META.get('HTTP_REFERER'))

@login_required
def signup(request):
  if request.method == 'POST':
    userform = UserForm(request.POST, request.FILES, instance=request.user)
    user = request.user
    if userform.is_valid():
      user = userform.save()
  userform = UserForm(instance=request.user)
  return render(request,'signup.html',
    {'form': userform})

from django.template.defaultfilters import striptags
def register(request):
  if request.user.is_authenticated():
    return redirect("/")
  elif request.method == "POST":
    response = {}
    response['status'] = 'FAIL'
    regform = RegisterForm(request.POST)
    if regform.is_valid():
      user = regform.save(False)
      user.avatar = "default.jpeg"
      user.save()
      response['status'] = 'OK'
      response['redirect'] = '/'
    else:
      response['info'] = []
      for error in regform.errors.iteritems():
        response['info'].append([unicode(striptags(error[0])), unicode(striptags(error[1]))])
    return HttpResponse(json.dumps(response), 'application/json')
  else:
    regform = RegisterForm()
    return render(request, 'register.html', {'regform' : regform})

@login_required
def ask(request):
  data = {}
  if request.method == "POST":
    form = AskForm(request.POST, user=request.user)
    if form.is_valid() and \
       len(form.cleaned_data['tags'].split(', ')) <= 3 and \
       Question.objects.filter(title=form.cleaned_data['title']).count() == 0:
         
      tags = form.cleaned_data['tags'].split(', ')
      q = form.save()
      for tag_name in tags:
        tag = Tag(word=tag_name)
        if Tag.objects.filter(word=tag_name).count() == 0:
          tag.save()
          q.tags.add(tag)
      q.save()
      data['status'] = 'OK'
      data['redirect'] = '/question?id=' + str(q.id)
    else:
      data['status'] = 'error'
      if not form.is_valid():
        data['message'] = 'Fill all fields in the appropriate way!'
      elif len(form.cleaned_data['tags'].split(', ')) > 3:
        data['message'] = "You should use 3 tags maximum!"
      elif Question.objects.filter(title=form.cleaned_data['title']).count() != 0:
        data['message'] = "There is already a question with this title. Please, use another one."
    return HttpResponse(json.dumps(data), content_type='application/json')
  else:
    form = AskForm()
    return render(request, 'ask.html', {'form' : form})

@login_required
def delete_question(request):
  q_id = request.POST.get('id', False)
  if q_id:
    q = Question.objects.filter(id=q_id).first()
    if q and q.author == request.user:
      q.delete()
  return redirect("/")
  
@login_required
def set_correct(request):
  answ_id = request.POST.get('id', False)
  answer = Answer.objects.get(id=answ_id)
  json_data = {}
  if answ_id and answer \
     and (answer.question.author == request.user):
    json_data['status'] = 'OK'
    is_correct = (request.POST.get('type', False) == 'correct')
    if is_correct and \
       answer.question.answer_set.filter(is_true=True).count() == 0:
      json_data['answ_is_correct'] = True
      answer.is_true = True
      answer.save()
    elif not is_correct:
      json_data['answ_is_correct'] = False
      answer.is_true = False
      answer.save()
    else:
      json_data['status'] = 'error'
      json_data['message'] = 'You have already chosen the correct answer. '
  else:
    json_data['status'] = 'error'
    json_data['message'] = 'Wrong request. '
    if not answer:
      json_data['message'] = json_data['message'] + \
        "Wrong answer_id. "
    elif answer.question.author != request.user:
      json_data['message'] = json_data['message'] + \
        "You have no permission because you are not the question's author. "
  return HttpResponse(json.dumps(json_data), content_type='application/json')

@login_required
def like(request):
  q_id = request.POST.get('id', False)
  q = Question.objects.filter(id=q_id).first()
  rec_type = request.POST.get('type', False)
  response_data = {}
  if rec_type == 'like' and q and q.like_set.filter(user = request.user).count() == 0:
    q.rating = q.rating + 1
    q.save()
    Like(question = q, user = request.user).save()
    response_data['status'] = 'dislike'
  elif rec_type == 'dislike' and q and q.like_set.filter(user = request.user).count() == 1:
    q.rating = q.rating - 1
    q.save()
    Like.objects.filter(question = q, user = request.user).delete()
    response_data['status'] = 'like'
  if q:
    response_data['rating'] = q.rating
  return HttpResponse(json.dumps(response_data), content_type="application/json")


@login_required
def tag_list(request, tagname):
  url = request.path
  pageid = 1
  if request.GET.get('page'):
    pageid = int(request.GET.get('page'))
  if pageid < 1:
    pageid = 1
    
  tag = Tag.objects.get(word=tagname)
  question_list = tag.question_set.order_by('-rating')
  paginator = Paginator(question_list, 20)
  
  try:
    questions = paginator.page(pageid)
  except EmptyPage:
    questions = paginator.page(paginator.num_pages)

  pagenum_minus_one = questions.paginator.num_pages - 1
  params = {
    'question_list':questions,
    'tag':tag,
    'pagenum_minus_one': pagenum_minus_one,
    'url':url
  }
  return render(request, 'tag_list.html', params)

