from django.core.cache import get_cache
from say.models import Tag, Question
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count
import datetime

memcached = get_cache('default')
class Command(BaseCommand):
  help = "Init best_tags memcache"
  def handle(self, *args, **kwargs):
    tags = Tag.objects.annotate(qnum=Count('question')).order_by('-qnum')[:20]
    memcached.set('best_tags', tags, 60*60*60)
