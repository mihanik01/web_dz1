from django.core.cache import get_cache
from say.models import User, Question, Answer
from django.core.management.base import BaseCommand, CommandError
import datetime

memcached = get_cache('default')
class Command(BaseCommand):
  help = "Init best_users memcache"
  def handle(self, *args, **kwargs):
    users = set()
    #questions = Question.objects.filter(date__gte=datetime.date.today()-datetime.timedelta(days=7)).order_by('-rating')[:10]
    questions = Question.objects.filter(date__gte=datetime.date.today()-datetime.timedelta(days=60)).order_by('-rating')[:10]
    for q in questions:
      users.add(q.author)
    memcached.set('best_users', users, 60*60*60)
