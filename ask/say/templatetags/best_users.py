from django import template
from django.core.cache import get_cache
  
register = template.Library()
memcached = get_cache('default')

@register.inclusion_tag("best_users.html")
def best_users():
  return {'best_users' : memcached.get('best_users')}
