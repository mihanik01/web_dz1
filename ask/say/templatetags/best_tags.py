from django import template
from django.core.cache import get_cache
  
register = template.Library()
memcached = get_cache('default')

@register.inclusion_tag("best_tags.html")
def best_tags():
  return {'best_tags' : memcached.get('best_tags')}
