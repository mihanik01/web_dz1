from django import template
  
register = template.Library()

@register.inclusion_tag("question_info.html")
def question_info(question):
  return {'question' : question}
